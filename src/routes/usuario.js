module.exports = function(application) {

    application.post('/login', function(req, res){
        application.src.controllers.usuario.login(application, req, res);
    });

    application.post('/insertLogin', function(req, res){
        application.src.controllers.usuario.cadastrar(application, req, res);
    });

}