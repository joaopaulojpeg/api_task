module.exports = function(application) {

    application.get('/tarefa/:usuario_id', function(req, res){
        application.src.controllers.tarefa.list(application, req, res);
    });

    application.post('/tarefa', function(req, res){
        application.src.controllers.tarefa.insert(application, req, res);
    });

    application.put('/tarefa/:tarefa_id', function(req, res){
        console.log('aqui')
        application.src.controllers.tarefa.update(application, req, res);
    });

    application.delete('/tarefa', function(req, res){
        application.src.controllers.tarefa.delete(application, req, res);
    });

    application.post('/tarefaConcluida', function(req, res){
        application.src.controllers.tarefa.concluida(application, req, res);
    });

}