function tarefa() { }

tarefa.prototype.list = async (req, res) => {
    var sql = `SELECT tarefa_id as id, descricao as title, 
    cast(data_ini AS timestamp) + INTERVAL '3' HOUR  as "start",
    cast(data_fim AS timestamp) + INTERVAL '3' HOUR  as "end",
                CASE 
                when cast(data_fim AS timestamp) + INTERVAL '3' HOUR < cast(NOW() AS timestamp) - INTERVAL '3' and status != 2 THEN 3
                ELSE status END status
                FROM tarefa
        WHERE
            usuario_id = $1
			ORDER BY status  DESC`;
    var values = [req.params.usuario_id]
    return new Promise((resolve, reject) => {

        global.pool.query( sql, values)
        .then( (res) => {
            var response
            if(res) response = res.rows

            resolve(response)
        })
        .catch(err => reject(err.stack));
    });
};


tarefa.prototype.insert = async (req, res) => {
    var sql = `INSERT INTO tarefa
                (usuario_id, descricao, data_ini, data_fim)
                VALUES
                ($1, $2, $3, $4)`;
    console.log(req.body.usuario_id, req.body.descricao, req.body.data_ini, req.body.data_fim)
    var values = [req.body.usuario_id, req.body.descricao, req.body.data_ini, req.body.data_fim]
    return new Promise((resolve, reject) => {

        global.pool.query( sql, values)
        .then( (res) => {
            var response = {res: true, msg: "Tarefa cadastrada com sucesso"}
            resolve(response)
        })
        .catch(err => reject(err.stack));
    });
};

tarefa.prototype.update = async (req, res) => {
    var sql = `UPDATE tarefa SET
                descricao = $1, data_ini = $2, data_fim = $3
                WHERE
                tarefa_id = $4`;
    
    var values = [req.body.descricao, req.body.dt_ini, req.body.dt_fim, req.params.tarefa_id]
    return new Promise((resolve, reject) => {

        global.pool.query( sql, values)
        .then( (res) => {
            var response = {res: true, msg: "Tarefa editada com sucesso"}
            resolve(response)
        })
        .catch(err => reject(err.stack));
    });
};

tarefa.prototype.delete = async (req, res) => {
    var sql = `DELETE FROM tarefa WHERE tarefa_id = $1`;
    
    var values = [req.body.tarefa_id]
    return new Promise((resolve, reject) => {
        console.log(req.body.tarefa_id)
        global.pool.query( sql, values)
        .then( (res) => {
            var response = {res: true, msg: "Tarefa removida com sucesso"}
            resolve(response)
        })
        .catch(err => reject(err.stack));
    });
};

tarefa.prototype.concluida = async (req, res) => {
    var sql = `UPDATE tarefa SET status = 2 WHERE tarefa_id = $1`;
    
    var values = [req.body.tarefa_id]
    return new Promise((resolve, reject) => {
        console.log(req.body.tarefa_id)
        global.pool.query( sql, values)
        .then( (res) => {
            var response = {res: true, msg: "Sucesso ao concluir"}
            resolve(response)
        })
        .catch(err => reject(err.stack));
    });
};

module.exports = function () {
    return tarefa;
}