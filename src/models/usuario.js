function categoria() { }

categoria.prototype.login = async (req, res) => {
    var criptografar = require('../services/criptografar')

    var sql = `SELECT *
                FROM public.usuario
                WHERE
                    email = $1 AND senha = $2;`;
    var senhaCrypto = criptografar.cryptString(req.body.senha) 
    var values = [req.body.email, senhaCrypto]
    console.log(senhaCrypto, req.body.email)

    return new Promise(resolve => {
        console.log('entrou na proimise')
        global.pool.query( sql, values)
        .then( (res) => {
            var response
            if(res) response = res.rows
            console.log(res)
            resolve(response)
        })
        .catch(err => reject(err.stack));
    });

};

categoria.prototype.cadastrar = async (req, res) => {
    var criptografar = require('../services/criptografar')

    var sql = `INSERT INTO usuario
                    (nome, email, senha)
                    VALUES
                    ($1, $2, $3)`;
                    console.log(req.body)
    var senhaCrypto = criptografar.cryptString(req.body.senha) 
    var values = [req.body.nome, req.body.usuario, senhaCrypto]
    return new Promise((resolve, reject) => {

        global.pool.query( sql, values)
        .then( (res) => {
            resolve({res: true, msg: "Cadastrado com sucesso"})
        })
        .catch(err => resolve({res: false, msg: "Email já existe, insira outro"}));
    });

};

module.exports = function () {
    return categoria;
}