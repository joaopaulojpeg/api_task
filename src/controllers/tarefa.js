
module.exports.list = async (application, req, res) => {
    var newsModel = new application.src.models.tarefa();
    await newsModel.list(req)
    .then( resultado => {
        res.send(resultado)
    });
};

module.exports.insert = async (application, req, res) => {
    var newsModel = new application.src.models.tarefa();
    await newsModel.insert(req)
    .then( resultado => {
        res.send(resultado)
    });
};

module.exports.update = async (application, req, res) => {
    var newsModel = new application.src.models.tarefa();
    await newsModel.update(req)
    .then( resultado => {
        res.send(resultado)
    });
};

module.exports.delete = async (application, req, res) => {
    var newsModel = new application.src.models.tarefa();
    await newsModel.delete(req)
    .then( resultado => {
        res.send(resultado)
    });
};

module.exports.concluida = async (application, req, res) => {
    var newsModel = new application.src.models.tarefa();
    await newsModel.concluida(req)
    .then( resultado => {
        res.send(resultado)
    });
};