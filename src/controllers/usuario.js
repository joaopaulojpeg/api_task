module.exports.login = async (application, req, res) => {
    var newsModel = new application.src.models.usuario();
    await newsModel.login(req)
    .then( resultado => {
        res.send(resultado)
    });
};

module.exports.cadastrar = async (application, req, res) => {
    var newsModel = new application.src.models.usuario();
    await newsModel.cadastrar(req)
    .then( resultado => {
        res.send(resultado)
    });
};
