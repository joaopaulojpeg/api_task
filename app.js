        
        var express = require('express');
        var consign = require('consign');
        const cors = require('cors');
        var app = require('./config/express')();
        // var chat = require('./src/services/chat')

        var corsOptions = {
            origin: '*',
            optionsSuccessStatus: 200, // For legacy browser support
            methods: "*"
        }
        
        app.use(cors(corsOptions));
        
        app.use(express.static('public'))


        var multer = require('multer');
        var upload = multer();

        app.use(express.json());

        app.use(express.urlencoded({ extended: true }));


        app.set('view engine', 'ejs');

        consign()
            .include('src/routes')
            .then('src/models')
            .then('src/controllers')
            .into(app);

        app.use(function (req, res, next) {

            // Website you wish to allow to connect
            res.setHeader('Access-Control-Allow-Origin', "*" );
            res.setHeader('Access-Control-Allow-Headers', 'X-Custom-Header, Upgrade-Insecure-Requests');

            res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
            res.setHeader('Access-Control-Allow-Credentials', false);

            next();
        });
        // var endereco = '';
        var endereco = require('http').createServer(app);
        
        endereco.listen(process.env.PORT || 3000, function(){
            console.log('APP rodando na porta process.env.PORT || 3000');
        });

            