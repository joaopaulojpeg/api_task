const express    = require('express');
const bodyParser = require('body-parser');

module.exports = () => {
  const app = express();

  // MIDDLEWARES
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
    extended: true
  }));


  const { Pool, Client } = require("pg");

  const connectionString = 'postgresql://tgptacvijduxjc:6922251b6358ca56b3c9290addc790ac753f3cf8556563848e1818b54e797446@ec2-23-20-124-77.compute-1.amazonaws.com:5432/df213l1kusb9fn'
  global.pool = new Client({
    connectionString,
    ssl: {
      rejectUnauthorized: false
    }
  })
  global.pool.connect((err, client, release) => {
    if (err) {
      return console.error('Error acquiring client', err.stack)
    }
  })
    
  return app;
};